1. The logger can be configured for what types of messages to print (warning messages, debug messages, info messages, etc..)

2. Maven Dependencies -> 
org.junit.jupiter.engine.extension -> 
DisabledCondition -> Line 31

3. Assertions.assertthrows checks that the enclosed code throws an exception

4.a. The SerialVersionUID is a version number associated with a serializable class to help with deserialization between a sender and a receiver
  
  b. The constructor is overridden to allow the programmer to pass a more detailed explanation of what happened along with the exception
  
  c.Overriding other exceptions was not necessary because the exceptions that were already overridden handled all possible exceptions in the program
  
5. The static block is used when setting the properties of the logger using the logger.properties file (initializing the logger)

6. .md files are markdown files that save plain text but include inline comments that format the file. It is related to bitbucket in that bitbucket uses markdown for file formatting

7. The problem is that timeNow is never instantiated, and so the function throws a NullPointerException, but the assertthrows() method is expecting a TimerException. The solution is to instantiate timeNow to an impossible value (such as -1)

8. assertThrows() handles multiple exceptions. In our case, it is only expecting a TimerException, so when a NullPointerException is thrown, the assert fails